import tt from './template/Template.vue'
import { parseComponent } from 'vue-template-compiler'


export default {
    install(Vue) {
        Vue.component("async-template", {
            template: '<div ref="template"></div>',
            components: {
                'element': null
            },
            methods: {
                relsolve(template) {
                    console.log(parseComponent(template))
                    console.log(template)
                    console.log(tt)
                }
            },
            created() {
                if (this.load && this.load instanceof Function) {
                    this.load(this.relsolve)
                }
            },
            // mixins: template,
            props: {
                load: Function
            }
        })
    }
}