import Container from '@/page/Container.vue'
import Home from '@/page/Home.vue'
import User from '@/page/future/User.vue'


import Assets from '@/page/assets/AssetsInfo.vue'
import AssetsCustomer from '@/page/assets/Customer.vue'


import Message from '@/page/message/Message.vue'
import Setting from '@/page/Setting'

import ContainerCommodity from '@/page/future/comm/Container.vue'
import Commodity from '@/page/future/comm/Commodity.vue'
import CommodityAdd from '@/page/future/comm/CommodityAdd.vue'
import CommodityPage from '@/page/future/comm/CommodityPage.vue'

import Login from '@/page/Login.vue'

import RouterContainer from '@/components/Container.vue'


export default [
    {
        path: '/',
        component: Container,
        meta: { name: "主页" },
        children: [
            {
                path: "home",
                component: Home,
            },
            {
                path: 'commodity',
                component: ContainerCommodity,
                meta: { name: '商品信息' },
                children: [
                    {
                        path: "add/:id",
                        component: CommodityAdd,
                        meta: { name: "添加商品" }
                    },
                    {
                        path: "page/:id",
                        component: CommodityPage,
                        meta: { name: "页面管理" }
                    },
                    {
                        path: ':id',
                        component: Commodity,
                        meta: { name: "商品管理" }
                    },
                ]
            },
            {
                path: 'assets',
                component: RouterContainer,
                meta: { name: '资源管理' },
                children: [
                    {
                        path: '',
                        component: Assets
                    },
                    {
                        path: 'details',
                        component: AssetsCustomer,
                        meta: { name: '资源详情' }
                    }
                ]
            },
            {
                path: "user",
                component: User,
                meta: { name: '用户管理' }
            },
            {
                path: 'message',
                component: Message,
                meta: { name: "消息处理" }
            },
            {
                path: "setting",
                component: Setting,
                meta: { name: "配置管理" }
            }
        ]
    },
    {
        path: '/login',
        component: Login
    }
]