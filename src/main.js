import Vue from 'vue'
import Router from 'vue-router'
import App from './App.vue'
import route from './route'
import Haier from './haier'
import Template from './template'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(Router)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Haier, Router)
Vue.use(Template)

new Vue({
  render: h => h(App),
  router: new Router({routes: route})
}).$mount('#app')
